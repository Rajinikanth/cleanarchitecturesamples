package com.gni.cleansampleapp.domain.usecases

import com.gni.cleansampleapp.data.SampleRepositoryInterface
import com.gni.cleansampleapp.domain.mapper.SampleMapper
import com.gni.cleansampleapp.presenter.entities.SampleSources
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers


class SampleUseCase(private val userRepository: SampleRepositoryInterface){

    fun getDummyData(): Observable<SampleSources> {
        return userRepository.getDummyData().map {
            SampleMapper.toSampleSource(it)
        }.observeOn(AndroidSchedulers.mainThread())
    }

}