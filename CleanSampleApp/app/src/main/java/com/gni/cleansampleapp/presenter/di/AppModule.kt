package com.gni.cleansampleapp.presenter.di

import com.gni.cleansampleapp.data.SampleRepository
import com.gni.cleansampleapp.data.SampleRepositoryInterface
import com.gni.cleansampleapp.domain.usecases.SampleUseCase
import com.gni.cleansampleapp.presenter.viewmodels.SampleViewModel
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module


private const val GET_SAMPLE_USECASE = "getSampleUseCase"

val mViewModels = module {
    viewModel {
        SampleViewModel(getSampleUseCase = get(GET_SAMPLE_USECASE))
    }
}

val mUseCaseModules = module {
    factory(name = "getSampleUseCase") { SampleUseCase(userRepository = get()) }
}


val mRepositories = module {
    single(name = "remote") { SampleRepository()  as SampleRepositoryInterface }
}