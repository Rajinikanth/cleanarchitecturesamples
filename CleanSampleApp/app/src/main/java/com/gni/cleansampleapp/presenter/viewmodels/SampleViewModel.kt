package com.gni.cleansampleapp.presenter.viewmodels

import android.annotation.SuppressLint
import androidx.lifecycle.MutableLiveData
import com.gni.cleansampleapp.domain.usecases.SampleUseCase
import com.gni.cleansampleapp.presenter.common.BaseViewModel
import com.gni.cleansampleapp.presenter.entities.SampleSources


class SampleViewModel(private val getSampleUseCase: SampleUseCase) : BaseViewModel() {

    var mSampleData = MutableLiveData<SampleSources>()


    @SuppressLint("CheckResult")
    fun fetchSampleData() {

        val disposable =  getSampleUseCase.getDummyData().subscribe({data ->
            print(data)
            mSampleData.value = data
        },{
            //Error while changing email
        })
        addDisposable(disposable)

    }

    fun getSampleLiveData() = mSampleData
}