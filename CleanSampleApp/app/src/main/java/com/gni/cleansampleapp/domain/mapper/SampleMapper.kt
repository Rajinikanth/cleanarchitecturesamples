package com.gni.cleansampleapp.domain.mapper

import com.gni.cleansampleapp.data.model.Sample
import com.gni.cleansampleapp.presenter.entities.SampleSources

object SampleMapper {
    fun toSampleSource(s: Sample): SampleSources {
        return SampleSources(s.text1,s.text2)
    }
}