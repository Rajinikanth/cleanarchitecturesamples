package com.gni.cleansampleapp.data

import com.gni.cleansampleapp.data.model.Sample
import io.reactivex.Observable

interface SampleRepositoryInterface {
    fun getDummyData(): Observable<Sample>
}