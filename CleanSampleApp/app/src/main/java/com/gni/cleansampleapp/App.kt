package com.gni.cleansampleapp

import android.app.Application
import com.gni.cleansampleapp.presenter.di.mRepositories
import com.gni.cleansampleapp.presenter.di.mUseCaseModules
import com.gni.cleansampleapp.presenter.di.mViewModels
import org.koin.android.ext.android.startKoin

class App : Application(){
    override fun onCreate() {
        super.onCreate()
        loadDependencies()
    }

    private fun loadDependencies(){
        startKoin(this,
            listOf(mViewModels, mRepositories, mUseCaseModules))
    }

}