package com.gni.cleansampleapp.presenter.entities

data class SampleSources(
    var text1: String? = null,
    var text2: String? = null

)

data class NewsPublisher(
    var id: Int,
    var name: String? = null
)