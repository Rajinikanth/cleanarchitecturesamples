package com.gni.cleansampleapp.data

import com.gni.cleansampleapp.data.model.Sample
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers

class SampleRepository : SampleRepositoryInterface {

    override fun getDummyData(): Observable<Sample> {
        return Observable.create<Sample> { emitter ->
            Thread.sleep(2000)
            emitter.onNext(Sample("RAJINI", "KANTH"))
        }.subscribeOn(Schedulers.io())
    }
}