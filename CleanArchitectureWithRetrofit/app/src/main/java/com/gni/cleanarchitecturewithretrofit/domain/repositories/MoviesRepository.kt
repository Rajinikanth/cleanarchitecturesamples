package com.gni.cleanarchitecturewithretrofit.domain.repositories

import com.gni.cleanarchitecturewithretrofit.data.entities.NewsSourcesEntity
import io.reactivex.Observable

interface MoviesRepository {
    fun getMovieList():Observable<NewsSourcesEntity>
}