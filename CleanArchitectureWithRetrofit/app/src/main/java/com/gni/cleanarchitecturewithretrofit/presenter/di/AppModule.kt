package com.gni.cleanarchitecturewithretrofit.presenter.di

import com.gni.cleanarchitecturewithretrofit.data.RemoteImpl
import com.gni.cleanarchitecturewithretrofit.data.SampleRepositoryImpl
import com.gni.cleanarchitecturewithretrofit.data.api.MovieListApi
import com.gni.cleanarchitecturewithretrofit.data.api.createNetworkClient
import com.gni.cleanarchitecturewithretrofit.domain.repositories.MoviesRepository
import com.gni.cleanarchitecturewithretrofit.domain.usecases.SampleUseCase
import com.gni.cleanarchitecturewithretrofit.presenter.viewmodels.SampleViewModel
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module
import retrofit2.Retrofit


private const val GET_SAMPLE_USECASE = "getSampleUseCase"
private const val RETROFIT_INSTANCE = "Retrofit"
private const val API = "Api"

val mViewModels = module {
    viewModel {
        SampleViewModel(getSampleUseCase = get(GET_SAMPLE_USECASE))
    }
}

val mUseCaseModules = module {
    factory(name = "getSampleUseCase") { SampleUseCase(userRepository = get()) }
}


val mNetworkModules = module {
    single(name = RETROFIT_INSTANCE) { createNetworkClient() }
    single(name = API) { (get(RETROFIT_INSTANCE) as Retrofit).create(MovieListApi::class.java) }
}


val mRepositories = module {
    single(name = "remote") { RemoteImpl(api = get(API)) }
    single { SampleRepositoryImpl(api = get("remote")) as MoviesRepository }
}
