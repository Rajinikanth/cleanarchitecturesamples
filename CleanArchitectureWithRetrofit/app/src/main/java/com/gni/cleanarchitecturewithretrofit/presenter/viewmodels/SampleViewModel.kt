package com.gni.cleanarchitecturewithretrofit.presenter.viewmodels

import android.annotation.SuppressLint
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.gni.cleanarchitecturewithretrofit.domain.usecases.SampleUseCase
import com.gni.cleanarchitecturewithretrofit.presenter.common.BaseViewModel
import com.gni.cleanarchitecturewithretrofit.presenter.entities.SampleSources


class SampleViewModel(private val getSampleUseCase: SampleUseCase) : BaseViewModel() {

    var mSampleData = MutableLiveData<SampleSources>()


    @SuppressLint("CheckResult")
    fun fetchSampleData() {

        val disposable =  getSampleUseCase.getDummyData().subscribe({data ->
            print(data)
            mSampleData.value = data
        },{
            Log.d("Rajini","Error $it")
        })
        addDisposable(disposable)

    }

    fun getSampleLiveData() = mSampleData
}