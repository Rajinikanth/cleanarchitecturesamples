package com.gni.cleanarchitecturewithretrofit.data

import com.gni.cleanarchitecturewithretrofit.data.entities.NewsSourcesEntity
import io.reactivex.Observable

interface SampleRepositoryInterface {
    fun getDummyData(): Observable<NewsSourcesEntity>
}