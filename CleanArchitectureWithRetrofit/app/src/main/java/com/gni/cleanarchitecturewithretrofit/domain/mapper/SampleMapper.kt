package com.gni.cleanarchitecturewithretrofit.domain.mapper

import com.gni.cleanarchitecturewithretrofit.presenter.entities.SampleSources
import com.gni.cleanarchitecturewithretrofit.data.entities.NewsSourcesEntity

object SampleMapper {
    fun toSampleSource(s: NewsSourcesEntity): SampleSources {
        return SampleSources("RAJINI...", "KANTH..")
    }
}