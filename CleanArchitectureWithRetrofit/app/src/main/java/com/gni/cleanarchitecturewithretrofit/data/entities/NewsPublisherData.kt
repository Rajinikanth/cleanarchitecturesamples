package com.gni.cleanarchitecturewithretrofit.data.entities


import com.google.gson.annotations.SerializedName


data class NewsPublisherData(
        @SerializedName("id") var id: Int = 0,
        @SerializedName("name") var name: String? = null,
        @SerializedName("description") var description: String? = null,
        @SerializedName("url") var url: String? = null,
        @SerializedName("category") var category: String? = null)