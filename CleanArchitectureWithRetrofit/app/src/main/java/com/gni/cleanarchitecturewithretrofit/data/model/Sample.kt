package com.gni.cleanarchitecturewithretrofit.data.model

data class Sample(val text1: String, val text2: String)
