package com.gni.cleanarchitecturewithretrofit.data.entities

import com.gni.cleanarchitecturewithretrofit.data.entities.NewsPublisherData
import com.google.gson.annotations.SerializedName


data class NewsSourcesEntity(
    @SerializedName("status") var status: String? = null,
    @SerializedName("articles") var articles: List<NewsPublisherData> = emptyList()
)
