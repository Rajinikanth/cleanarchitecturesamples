package com.gni.cleanarchitecturewithretrofit.data

import com.gni.cleanarchitecturewithretrofit.data.entities.NewsSourcesEntity
import com.gni.cleanarchitecturewithretrofit.domain.repositories.MoviesRepository
import io.reactivex.Observable

class SampleRepositoryImpl(private val api: RemoteImpl): MoviesRepository {

    override fun getMovieList(): Observable<NewsSourcesEntity> {
        return api.getDummyData()
    }

}