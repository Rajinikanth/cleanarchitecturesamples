package com.gni.cleanarchitecturewithretrofit.data

import com.gni.cleanarchitecturewithretrofit.data.api.MovieListApi
import com.gni.cleanarchitecturewithretrofit.data.entities.NewsSourcesEntity
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers

class RemoteImpl(private val api: MovieListApi) : SampleRepositoryInterface {
    override fun getDummyData(): Observable<NewsSourcesEntity> {
        return Observable.create<NewsSourcesEntity> { emitter ->

            api.getData().map {
                emitter.onNext(it)
            }.onErrorReturn {
                emitter.onError(it)
            }.subscribe()

        }.subscribeOn(Schedulers.io())
    }
}