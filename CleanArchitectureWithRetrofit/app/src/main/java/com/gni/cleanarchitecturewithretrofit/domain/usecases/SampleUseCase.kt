package com.gni.cleanarchitecturewithretrofit.domain.usecases


import com.gni.cleanarchitecturewithretrofit.domain.mapper.SampleMapper
import com.gni.cleanarchitecturewithretrofit.domain.repositories.MoviesRepository
import com.gni.cleanarchitecturewithretrofit.presenter.entities.SampleSources
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers


class SampleUseCase(private val userRepository: MoviesRepository){

    fun getDummyData(): Observable<SampleSources> {
        return userRepository.getMovieList().map {
            SampleMapper.toSampleSource(it)
        }.observeOn(AndroidSchedulers.mainThread())
    }

}