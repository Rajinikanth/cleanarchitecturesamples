package com.gni.cleanarchitecturewithretrofit

import android.app.Application
import com.gni.cleanarchitecturewithretrofit.presenter.di.mNetworkModules
import com.gni.cleanarchitecturewithretrofit.presenter.di.mRepositories
import com.gni.cleanarchitecturewithretrofit.presenter.di.mUseCaseModules
import com.gni.cleanarchitecturewithretrofit.presenter.di.mViewModels
import org.koin.android.ext.android.startKoin

class App : Application(){
    override fun onCreate() {
        super.onCreate()
        loadDependencies()
    }

    private fun loadDependencies(){
        startKoin(this,
            listOf(mNetworkModules, mViewModels, mRepositories, mUseCaseModules))
    }

}