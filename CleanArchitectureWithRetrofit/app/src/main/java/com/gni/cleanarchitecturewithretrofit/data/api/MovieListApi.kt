package com.gni.cleanarchitecturewithretrofit.data.api

import com.gni.cleanarchitecturewithretrofit.data.entities.NewsSourcesEntity
import io.reactivex.Observable
import retrofit2.http.GET

interface MovieListApi{

    @GET("top-headlines?country=us")
    fun getData():Observable<NewsSourcesEntity>
}